#include "ns3/applications-module.h"
#include "ns3/config-store.h"
#include "ns3/core-module.h"
#include "ns3/event-id.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/gnuplot.h"
#include "ns3/internet-apps-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/stats-module.h"

// Default Network Topology
//
//      192.168.1.0
// n0 -------------- n1
//    point-to-point
//
//
//      192.168.2.0
// n2 -------------- n3
//    point-to-point
//

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("4NodeHandshake-tcp");

static std::map<uint32_t, bool> firstCwnd;
static std::map<uint32_t, bool> firstSshThr;
static std::map<uint32_t, bool> firstRtt;
static std::map<uint32_t, Ptr<OutputStreamWrapper>>
    cWndStream; //!< Congestion window output stream.
static std::map<uint32_t, Ptr<OutputStreamWrapper>> rttStream;
static std::map<uint32_t, Ptr<OutputStreamWrapper>> ssThreshStream;
static std::map<uint32_t, uint32_t> cWndValue;
static std::map<uint32_t, uint32_t> ssThreshValue;

static uint32_t
GetNodeIdFromContext(std::string context)
{
    const std::size_t n1 = context.find_first_of('/', 1);
    const std::size_t n2 = context.find_first_of('/', n1 + 1);
    return std::stoul(context.substr(n1 + 1, n2 - n1 - 1));
}

/**
 * RTT tracer.
 *
 * \param context The context.
 * \param oldval Old value.
 * \param newval New value.
 */
static void
RttTracer(std::string context, Time oldval, Time newval)
{
    uint32_t nodeId = GetNodeIdFromContext(context);

    if (firstRtt[nodeId])
    {
        *rttStream[nodeId]->GetStream() << "0.0 " << oldval.GetSeconds() << std::endl;
        firstRtt[nodeId] = false;
    }
    *rttStream[nodeId]->GetStream()
        << Simulator::Now().GetSeconds() << " " << newval.GetSeconds() << std::endl;
}

/**
 * Slow start threshold tracer.
 *
 * \param context The context.
 * \param oldval Old value.
 * \param newval New value.
 */
static void
SsThreshTracer(std::string context, uint32_t oldval, uint32_t newval)
{
    uint32_t nodeId = GetNodeIdFromContext(context);

    if (firstSshThr[nodeId])
    {
        *ssThreshStream[nodeId]->GetStream() << "0.0 " << oldval << std::endl;
        firstSshThr[nodeId] = false;
    }
    *ssThreshStream[nodeId]->GetStream()
        << Simulator::Now().GetSeconds() << " " << newval << std::endl;
    ssThreshValue[nodeId] = newval;

    if (!firstCwnd[nodeId])
    {
        *cWndStream[nodeId]->GetStream()
            << Simulator::Now().GetSeconds() << " " << cWndValue[nodeId] << std::endl;
    }
}

/**
 * Slow start threshold trace connection.
 *
 * \param ssthresh_tr_file_name Slow start threshold trace file name.
 * \param nodeId Node ID.
 */
static void
TraceSsThresh(std::string ssthresh_tr_file_name, uint32_t nodeId)
{
    AsciiTraceHelper ascii;
    ssThreshStream[nodeId] = ascii.CreateFileStream(ssthresh_tr_file_name);
    Config::Connect("/NodeList/" + std::to_string(nodeId) +
                        "/$ns3::TcpL4Protocol/SocketList/0/SlowStartThreshold",
                    MakeCallback(&SsThreshTracer));
}

static void
CwndTracer(std::string context, uint32_t oldval, uint32_t newval)
{
    uint32_t nodeId = GetNodeIdFromContext(context);

    if (firstCwnd[nodeId])
    {
        *cWndStream[nodeId]->GetStream() << "0.0 " << oldval << std::endl;
        firstCwnd[nodeId] = false;
    }
    *cWndStream[nodeId]->GetStream() << Simulator::Now().GetSeconds() << " " << newval << std::endl;
    cWndValue[nodeId] = newval;

    if (!firstSshThr[nodeId])
    {
        *ssThreshStream[nodeId]->GetStream()
            << Simulator::Now().GetSeconds() << " " << ssThreshValue[nodeId] << std::endl;
    }
}

/**
 * RTT trace connection.
 *
 * \param rtt_tr_file_name RTT trace file name.
 * \param nodeId Node ID.
 */
static void
TraceRtt(std::string rtt_tr_file_name, uint32_t nodeId)
{
    AsciiTraceHelper ascii;
    rttStream[nodeId] = ascii.CreateFileStream(rtt_tr_file_name);
    Config::Connect("/NodeList/" + std::to_string(nodeId) + "/$ns3::TcpL4Protocol/SocketList/0/RTT",
                    MakeCallback(&RttTracer));
}

static void
TraceCwnd(std::string cwnd_tr_file_name, uint32_t nodeId)
{
    AsciiTraceHelper ascii;
    cWndStream[nodeId] = ascii.CreateFileStream(cwnd_tr_file_name);
    Config::Connect("/NodeList/" + std::to_string(nodeId) +
                        "/$ns3::TcpL4Protocol/SocketList/0/CongestionWindow",
                    MakeCallback(&CwndTracer));
}

int
main(int argc, char* argv[])
{
    CommandLine cmd(__FILE__);
    cmd.Parse(argc, argv);

    Time::SetResolution(Time::NS);
    LogComponentEnable("4NodeHandshake-tcp", LOG_LEVEL_INFO);

    uint16_t maxBytes = 0;
    // double timeInterval = 0.1;
    u_int64_t txPackets1 = 0;
    u_int64_t rxPackets1 = 0;
    u_int64_t rxBytes1 = 0;
    u_int64_t lost1 = 0;
    double delaySum1 = 0;
    double RTT1 = 0;
    u_int64_t txPackets2 = 0;
    u_int64_t rxPackets2 = 0;
    u_int64_t rxBytes2 = 0;
    u_int64_t lost2 = 0;
    double delaySum2 = 0;
    double RTT2 = 0;

    u_int64_t numRuns = 1;
    RngSeedManager::SetSeed(42);

    for (uint64_t j = 0; j < numRuns; j++)
    {
        // Nodes of the first network
        NodeContainer fNodes;
        fNodes.Create(2);

        // Nodes of the second network
        NodeContainer sNodes;
        sNodes.Create(2);

        // Point-to-point setup
        PointToPointHelper pointToPoint;
        pointToPoint.SetDeviceAttribute("DataRate", StringValue("1Mbps"));
        pointToPoint.SetChannelAttribute("Delay", StringValue("1ms"));

        // Install the nodes on the first point-to-point network
        NetDeviceContainer fDevices;
        fDevices = pointToPoint.Install(fNodes);

        // Install the nodes on the second point-to-point network
        NetDeviceContainer sDevices;
        sDevices = pointToPoint.Install(sNodes);

        // Install the internet stack on all the nodes
        InternetStackHelper stack;
        stack.Install(fNodes);
        stack.Install(sNodes);

        // Assign IPv4 addresses
        Ipv4AddressHelper address;
        address.SetBase("192.168.1.0", "255.255.255.0");
        Ipv4InterfaceContainer fInterfaces = address.Assign(fDevices);
        address.SetBase("192.168.2.0", "255.255.255.0");
        Ipv4InterfaceContainer sInterfaces = address.Assign(sDevices);

        // Set a receive error model on the first network device of the second node of the first
        // network
        Ptr<PointToPointNetDevice> p2pSender = DynamicCast<PointToPointNetDevice>(fDevices.Get(1));
        Ptr<RateErrorModel> errorModel = CreateObject<RateErrorModel>();
        errorModel->SetAttribute("ErrorRate", DoubleValue(0.0001));
        p2pSender->SetReceiveErrorModel(errorModel);

        // Send Helper for the first network
        BulkSendHelper source("ns3::TcpSocketFactory",
                              InetSocketAddress(fInterfaces.GetAddress(1), 420));
        // Set the amount of data to send in bytes.  Zero is unlimited.
        source.SetAttribute("MaxBytes", UintegerValue(maxBytes));
        source.SetAttribute("SendSize", UintegerValue(1024));
        ApplicationContainer f_serverApps = source.Install(fNodes.Get(0));
        f_serverApps.Start(Seconds(1.0));
        f_serverApps.Stop(Seconds(101.0));

        //
        // Create a PacketSinkApplication and install it on node 1
        //
        PacketSinkHelper sink("ns3::TcpSocketFactory",
                              InetSocketAddress(Ipv4Address::GetAny(), 420));
        ApplicationContainer f_clientApps = sink.Install(fNodes.Get(1));
        f_clientApps.Start(Seconds(2.0));
        f_clientApps.Stop(Seconds(98.0));

        // Send Helper for the second network
        BulkSendHelper s_source("ns3::TcpSocketFactory",
                                InetSocketAddress(sInterfaces.GetAddress(1), 420));

        // Set the amount of data to send in bytes.  Zero is unlimited
        s_source.SetAttribute("MaxBytes", UintegerValue(maxBytes));
        s_source.SetAttribute("SendSize", UintegerValue(1024));
        ApplicationContainer s_serverApps = s_source.Install(sNodes.Get(0));
        s_serverApps.Start(Seconds(1.0));
        s_serverApps.Stop(Seconds(101.0));

        //
        // Create a PacketSinkApplication and install it on node 1
        //
        PacketSinkHelper s_sink("ns3::TcpSocketFactory",
                                InetSocketAddress(Ipv4Address::GetAny(), 420));
        ApplicationContainer s_clientApps = s_sink.Install(sNodes.Get(1));
        s_clientApps.Start(Seconds(2.0));
        s_clientApps.Stop(Seconds(98.0));

        // Flow monitor
        Ptr<FlowMonitor> flowMonitor;
        FlowMonitorHelper flowHelper;
        flowMonitor = flowHelper.InstallAll();

        Address fSource(fNodes.Get(0)->GetObject<Ipv4>()->GetAddress(1, 0).GetLocal());
        Address sSource(sNodes.Get(0)->GetObject<Ipv4>()->GetAddress(1, 0).GetLocal());

        firstCwnd[0] = firstCwnd[1] = true;
        Simulator::Schedule(Seconds(1 + 0.00001),
                            &TraceCwnd,
                            std::to_string(j) + "-first-cwnd.data",
                            fNodes.Get(0)->GetId());
        Simulator::Schedule(Seconds(1 + 0.00001),
                            &TraceCwnd,
                            std::to_string(j) + "-second-cwnd.data",
                            sNodes.Get(0)->GetId());
        Simulator::Schedule(Seconds(1 + 0.00001),
                            &TraceSsThresh,
                            std::to_string(j) + "-first-ssth.data",
                            fNodes.Get(0)->GetId());
        Simulator::Schedule(Seconds(1 + 0.00001),
                            &TraceSsThresh,
                            std::to_string(j) + "-second-ssth.data",
                            sNodes.Get(0)->GetId());
        Simulator::Schedule(Seconds(1 + 0.00001),
                            &TraceRtt,
                            std::to_string(j) + "-first-rtt.data",
                            fNodes.Get(0)->GetId());
        Simulator::Schedule(Seconds(1 + 0.00001),
                            &TraceRtt,
                            std::to_string(j) + "-second-rtt.data",
                            sNodes.Get(0)->GetId());

        Simulator::Stop(Seconds(300.0));
        Simulator::Run();

        flowMonitor->SerializeToXmlFile(std::to_string(j) + "-4-nodes-handshake-tcp.xml",
                                        true,
                                        true);
        // Data collection and nice plots
        flowMonitor->CheckForLostPackets();

        Ptr<Ipv4FlowClassifier> classifier =
            DynamicCast<Ipv4FlowClassifier>(flowHelper.GetClassifier());
        auto stats = flowMonitor->GetFlowStats();
        for (auto i = stats.begin(); i != stats.end(); ++i)
        {
            Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);
            if (t.sourceAddress == fSource)
            {
                txPackets1 += i->second.txPackets;
                rxBytes1 += i->second.rxBytes;
                rxPackets1 += i->second.rxPackets;
                lost1 += i->second.lostPackets;
                delaySum1 += i->second.delaySum.GetSeconds();
                RTT1 += i->second.delaySum.GetSeconds() / i->second.rxPackets;
            }

            if (t.sourceAddress == sSource)
            {
                txPackets2 += i->second.txPackets;
                rxPackets2 += i->second.rxPackets;
                rxBytes2 += i->second.rxBytes;
                lost2 += i->second.lostPackets;
                delaySum2 += i->second.delaySum.GetSeconds();
                RTT2 += i->second.delaySum.GetSeconds() / i->second.rxPackets;
            }
        }

        // Throughput Variance, Congestion Windows, TCP time, Buffer Size
        /*
        Ptr<PacketSink> sink1 = DynamicCast<PacketSink>(f_clientApps.Get(0));
        std::cout << "Total Bytes Received: " << sink1->GetTotalRx() << std::endl;

        Ptr<PacketSink> sink2 = DynamicCast<PacketSink>(s_clientApps.Get(0));
        std::cout << "Total Bytes Received: " << sink2->GetTotalRx() << std::endl;
        */

        Simulator::Destroy();
    }

    NS_LOG_INFO(" ----- FIRST FLOW -----");
    NS_LOG_INFO("Tx Packets: " << txPackets1 / numRuns);
    NS_LOG_INFO("Rx Packets: " << rxPackets1 / numRuns);
    NS_LOG_INFO("Throughput: " << rxBytes1 * 8.0 / 1000 / numRuns << " Kbps");
    NS_LOG_INFO("Lost Packets: " << (double)lost1 / numRuns);
    NS_LOG_INFO("Packet Loss Ratio: " << (double)lost1 / txPackets1);
    NS_LOG_INFO("Delay Sum: " << delaySum1 / numRuns);
    NS_LOG_INFO("RTT: " << RTT1 / numRuns);

    NS_LOG_INFO(std::endl << " ----- SECOND FLOW -----");
    NS_LOG_INFO("Tx Packets: " << txPackets2 / numRuns);
    NS_LOG_INFO("Rx Packets: " << rxPackets2 / numRuns);
    NS_LOG_INFO("Throughput: " << rxBytes2 * 8.0 / 1000 / numRuns << " Kbps");
    NS_LOG_INFO("Lost Packets: " << (double)lost2 / numRuns);
    NS_LOG_INFO("Packet Loss Ratio: " << (double)lost2 / txPackets2);
    NS_LOG_INFO("Delay Sum: " << delaySum2 / numRuns);
    NS_LOG_INFO("RTT: " << RTT2 / numRuns);

    return 0;
}